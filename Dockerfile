# Imagen base
FROM node:latest

#directorio de la app en el contenedor
WORKDIR /app

#copiado de archivos
ADD /build/default /app/build/default
ADD /server.js /app
ADD /package.json /app

# dependencias
RUN npm install

#Puerto que expongo
EXPOSE 300

#comando
CMD ["npm", "start"]
